package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func main() {
	var group []string

	points := generatePoints()

	bagTotal := 0
	badgeTotal := 0
	count := 0
	count2 := 0
	input, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer input.Close()
	scanner := bufio.NewScanner(input)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		bags := scanner.Text()
		compartments := splitString(bags)
		bagDuplicates, _ := findDuplicates(compartments)

		for _, v := range bagDuplicates {
			bagTotal += points[v]
		}

		group = append(group, bags)
		if len(group) == 3 {
			pair := []string{group[0], group[1]}
			dupes, _ := findDuplicates(pair)
			pair = []string{dupes, group[2]}
			dupes, _ = findDuplicates(pair)
			for _, v := range dupes {
				badgeTotal += points[v]
				count += 1
			}
			group = group[:0]
			count2 += 1
		}
	}

	fmt.Println("The total for bags is:", bagTotal)
	fmt.Println("The total for badges is:", badgeTotal, count, count2)
}

func splitString(s string) []string {
	var list []string
	var bag1 string
	var bag2 string
	for i, v := range s {
		if i < len(s)/2 {
			bag1 = bag1 + string(v)
		} else if i >= len(s)/2 {
			bag2 = bag2 + string(v)
		}
	}
	list = append(list, bag1, bag2)
	return list
}

func findDuplicates(s []string) (string, error) {
	var err error
	var shared string
	var r []rune
	bag1 := []rune(s[0])
	bag2 := []rune(s[1])

	for _, v1 := range bag1 {
		for _, v2 := range bag2 {
			if v2 == v1 {
				r = append(r, v1)
			}
		}
	}
	r = removeDuplicateValues(r)
	shared = string(r)
	return shared, err
}

func removeDuplicateValues(s []rune) []rune {
	keys := make(map[rune]bool)
	list := []rune{}

	for _, entry := range s {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

func generatePoints() map[rune]int {
	var l rune
	var i int
	r := make(map[rune]int)

	i = 1
	for l = 97; l%97 <= 25; l++ {
		r[l] = i
		i += 1
	}
	for l = 65; l%65 <= 25; l++ {
		r[l] = i
		i += 1
	}
	return r
}
