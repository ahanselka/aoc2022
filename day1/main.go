package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func removeElement(s []int, index int) []int {
	s[index] = s[len(s)-1]
	return s[:len(s)-1]
}

func contains(s []int, i int) int {
	for index, v := range s {
		if v == i {
			return index
		}
	}
	return 0
}

func findTop(s []int) int {
	var max int
	for _, item := range s {
		if item >= max {
			max = item
		}
	}
	return max
}

func findTopN(s []int, n int) []int {
	var topN []int
	var max int
	var index int
	for i := 0; i < n; i++ {
		max = findTop(s)
		topN = append(topN, max)
		index = contains(s, max)
		s = removeElement(s, index)
	}
	return topN
}

func addSlice(s []int) int {
	total := 0
	for _, number := range s {
		total += number
	}
	return total
}

func main() {
	var elfTotals []int
	var totalCalories int
	input, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer input.Close()
	scanner := bufio.NewScanner(input)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		calories, _ := strconv.Atoi(scanner.Text())
		if calories == 0 {
			elfTotals = append(elfTotals, totalCalories)
			totalCalories = 0
		}
		totalCalories += calories
	}
	max := findTop(elfTotals)
	fmt.Println("Max Calories: ", max)

	topN := findTopN(elfTotals, 3)

	fmt.Println("Top 3: ", topN)
	total := addSlice(topN)
	fmt.Println("Total: ", total)
}
