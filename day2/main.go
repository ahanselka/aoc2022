package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

var (
	scoreChart = map[string]int{
		"A": 1,
		"B": 2,
		"C": 3,
	}

	winningShapes = map[string]string{
		"A": "B",
		"B": "C",
		"C": "A",
	}

	losingShapes = map[string]string{
		"A": "C",
		"B": "A",
		"C": "B",
	}
)

func chooseShape(opponent string, self string) string {
	if self == "Y" {
		return opponent
	} else if self == "X" {
		return losingShapes[opponent]
	} else {
		return winningShapes[opponent]
	}
}

func rpsMatch(opponent string, self string) int {
	if opponent == self {
		return 3
	} else if (opponent == "A") && (self == "B") {
		return 6
	} else if (opponent == "B") && (self == "C") {
		return 6
	} else if (opponent == "C") && (self == "A") {
		return 6
	} else {
		return 0
	}
}

func main() {
	totalScore := 0
	input, err := os.Open("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer input.Close()
	scanner := bufio.NewScanner(input)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		opponent := string(scanner.Text()[0])
		self := string(scanner.Text()[2])
		selfShape := chooseShape(opponent, self)
		result := rpsMatch(opponent, selfShape)
		totalScore = totalScore + result + scoreChart[selfShape]
	}

	fmt.Println("Your total: ", totalScore)
}
